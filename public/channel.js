/* channel class. It is a container of filters. It also defines what filters are synced between each other */

export class Channel {
    constructor(filters, relationships) {
        this.filters = filters;
        this.relationships = relationships;
        this._validateRelationships();
        /* Create a simpler data structure: {filter: [other filters]} from the original structure of lists */
        this._filterToOthers = this._buildFilterMap();
    }

    syncRelationshipFilters(filterName) {
        /* when this is called it means that the filter triggered a change to all it's relationship filters*/
        for (const otherFilterName of this._filterToOthers[filterName]) {
            const newValues = this.filters[filterName].getValues();
            this.filters[otherFilterName].setValues(newValues);
        }
    }

    _buildFilterMap() {
        /* since it's an internal function, we know that _validationRelationShips() was called before this. */
        var res = {};
        var nRels = 0;
        for (const rel of this.relationships) {
            for (const filter of rel) {
                /* store for the current filter a list with all the others in the relationship except him*/
                res[filter] = rel.filter(word => word != filter);
                nRels += 1;
            }
        }
        console.log(`[Channel::_builFilterMap] Filter map was properly constructed. Got ${nRels} relationships.`);
        return res;
    }

    _validateRelationships() {
        /* checks that all the relationships are valid. No inexistent filters are provided here.
         * rels = [ [f1, f2], [f3, f5] ] with f1, f2, f3, f5 being names of existing filters
         * rels = [ [f1, f2], [f3, f2] ] will also trigger an error because f2 is in two relationships
         * It can be rewritten as rels = [ [f1, f2, f3] ]
         */

        var filtersSoFar = [];
        for (const rel of this.relationships) {
            if (!Array.isArray(rel)) {
                throw Error(`Relationship ${rel} of ${this.relationships} is not an array of filter names.`);
            }
            for (const filter of rel) {
                if (!(filter in this.filters)) {
                    throw Error(
                        `Filter '${filter}' of relationship ${rel} not in filters: ${Object.keys(this.filters)}`
                    );
                }
                if (filtersSoFar.includes(filter)) {
                    throw Error(
                        `Relationship ${rel} provides a filter ('${filter}') that is in ` +
                            `another rel too ${this.relationships}`
                    );
                }
                filtersSoFar.push(filter);
            }
        }
    }
}
