import { Channel } from "./channel.js";
import { MyFilter } from "./myfilter.js";

/* defining some globals mapping to DOM elements */
const FILTER_VALUE = "filterValue"; // these are elements that will trigger the on change
const FILTER_CLASS = "filter"; // these are the filters in the DOM that have a 1:1 relationship with oop filter objects

(function () {
    main();
})();

function getParentFilter(htmlFilterValue, lookupClassName) {
    // This finds the parent with the class name 'filter' (given as parameter)
    console.assert(lookupClassName != null, "cannot be null");
    var current = htmlFilterValue;
    // no need to do any stopping condition. It will simply fail if no parents are left (we get to <html>)
    // contains because the filters may have css classes as well
    while (!current.classList.contains(lookupClassName)) {
        current = current.parentElement;
    }
    return current;
}

function syncOnChange(channel, htmlFilterValue) {
    // this function is the handler for all changes in the main page

    // first step is to get the parent filter from this value/input that has changed
    const htmlParentFilter = getParentFilter(htmlFilterValue, FILTER_CLASS);
    const parentFilter = channel.filters[htmlParentFilter.id];
    const newValues = parentFilter.getHTMLValues();
    parentFilter.setValues(newValues);
    channel.syncRelationshipFilters(parentFilter.name);
    console.log(`[inputHandler] Element ${htmlFilterValue} of filter '${parentFilter.name}' triggered a change.`);
}

function main() {
    console.log("Calling main");
    const allFilterValues = document.getElementsByClassName(FILTER_VALUE);
    // define one test channel and two filters
    const filters = {
        f1: new MyFilter("f1", ["", "", ""]),
        f2: new MyFilter("f2", ["", "", ""]),
        f3: new MyFilter("f3", ["", "", ""]),
        f4: new MyFilter("f4", ["", "", ""]),
    };

    const channel = new Channel(filters, [
        ["f1", "f4"],
        ["f2", "f3"],
    ]);

    /* for all the filters values (changable values/inputs), we attach the same handler. */
    for (const value of allFilterValues) {
        value.oninput = function () {
            syncOnChange(channel, value);
        };
    }
}
