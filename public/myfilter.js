import { Filter } from "./filter.js";

export class MyFilter extends Filter {
    constructor(name, defaultValues) {
        super(name, defaultValues);
        // store references to the html objects
        this.htmlFilter = null;
        this.htmlInputs = null;
    }

    setValues(newValues) {
        if (!Array.isArray(newValues) || newValues.length != 3) {
            throw new Error(`[setValues-${this.name} New values is not an array or doesn't have 3 elems: ${newValues}`);
        }
        super.setValues(newValues);
    }

    syncHtml() {
        if (!this.htmlFilter || !this.htmlInputs) {
            // TODO: why aren't these maintained?
            this._validateHtml();
            // throw Error(`Filter of type MyFilter with id '${this.name}' didn't call _validateHtml first.`);
        }
        for (var i = 0; i < this.htmlInputs.length; i++) {
            this.htmlInputs[i].value = this.currentValues[i];
        }
        console.log(`[syncHtml-${this.name}] HTML synced. Values updated to: ${this.getValues()}`);
    }

    getHTMLValues() {
        if (!this.htmlFilter || !this.htmlInputs) {
            // TODO: why aren't these maintained?
            this._validateHtml();
            // throw Error(`Filter of type MyFilter with id '${this.name}' didn't call _validateHtml first.`);
        }
        var result = [];
        for (const htmlInput of this.htmlInputs) {
            result.push(htmlInput.value);
        }
        return result;
    }

    _validateHtml() {
        /*
        validate MyFilter
        We know it must have the following form:

        <div class="filter MyFilter (other css classes)" id="{this.name}">
            Some text <br />
            <input type="text" class="filterValue" ord=0> </input> <br />
            <input type="text" class="filterValue" ord=1> </input> <br />
            <input type="text" class="filterValue" ord=2> </input>
        </div>
        */
        this.htmlFilter = document.getElementById(this.name);
        if (!this.htmlFilter) {
            throw Error(`Filter of type MyFilter with id '${this.name}' does not exist in html. Create it!`);
        }
        if (!this.htmlFilter.classList.contains("filter") || !this.htmlFilter.classList.contains("MyFilter")) {
            throw Error(`Filter of type MyFilter with id '${this.name}' has no filter and MyFilter classes!`);
        }
        // look for the 3 input type texts now
        this.htmlInputs = [];
        for (const child of this.htmlFilter.children) {
            if (!child.hasAttribute("ord")) {
                continue;
            }
            const ord = child.getAttribute("ord");
            if (parseInt(ord) != this.htmlInputs.length || parseInt(ord) >= 3) {
                throw Error(`Filter of type MyFilter with id '${this.name}' has values in wrong order!`);
            }
            this.htmlInputs.push(child);
        }

        console.log(`[syncHtml-${this.name}] HTML Validated. Found html object and it's proper.`);
    }
}
