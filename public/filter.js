/* abstract method of filters. More specific filters (Range, Select etc.) will overwrite the methods. */
export class Filter {
    constructor(name, defaultValues) {
        /* there is a 1:1 relationship between the name and the html Id */
        this.name = name;
        this.currentValues = null;
        this._validateHtml();
        this.setValues(defaultValues);
    }

    setValues(newValues) {
        console.log(`[setValues-${this.name}] Old values: ${this.currentValues}. New values: ${newValues}.`);
        this.currentValues = newValues;
        this.syncHtml();
    }

    getValues() {
        console.log(`[getValues-${this.name}] -> ${this.currentValues}.`);
        return this.currentValues;
    }

    getHTMLValues() {
        /* This will get the values from the DOM whenever a changed is triggered. These values go in setValues later */
        throw new Error(`[syncHtml-${this.name}] Abstract method!`);
    }

    syncHtml() {
        /* This will update the html code for this filter based on the name */
        throw new Error(`[syncHtml-${this.name}] Abstract method!`);
    }

    _validateHtml() {
        /* This will validate the html code for this filter based on the name (i.e. it actually has the values) */
        throw new Error(`[syncHtml-${this.name}] Abstract method!`);
    }
}
