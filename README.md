<h1> Sync multiple DOM elements together </h1>

Basic blocks:

-   Filters
-   Filter Values (these trigger to changes and are filter-specific)
-   filters are contained in a 'channel' that defines the synchronization

Only a custom 'MyFilter' is implemented, which has 3 filter values (triggers), each of them being a input type text.

```
const filters = {
    f1: new MyFilter("f1", ["", "", ""]),
    f2: new MyFilter("f2", ["", "", ""]),
    f3: new MyFilter("f3", ["", "", ""]),
    f4: new MyFilter("f4", ["", "", ""]),
};

const mapping = [
    ["f1", "f4"],
    ["f2", "f3"],
]

const channel = new Channel(filters, mapping)

```

View at: [link](https://meehai.gitlab.io/js-synced-dom-elements/)
